export const Home = () => {
  const Home = document.createElement('div')
  Home.className = 'home'
  Home.innerHTML = `
      <div class="header"> 
         <div class="header-title">Home</div>
         <ul class="header-buttons">
            <li><a href="" class="header-link">For you</a></li>
            <li><a href="">Following</a></li>
         </ul>
      </div>
      <div class="post-list"></div>`
  const header = document.querySelector('header')

  return Home
}
