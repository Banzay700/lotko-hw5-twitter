import { navList } from './layouts.utils'
import logo from '../../assets/images/navIcons/mlogo.svg'

export const Navbar = () => {
  const Navbar = document.createElement('nav')

  Navbar.className = 'navbar'
  Navbar.innerHTML = `
   <div class="nav">
      <div class="nav-logo"><img src="${logo}" /></div>
      <ul class="nav-menu">
         ${navList
           .map(
             ({ name, path }) =>
               `<li class="nav-item"> <a href="" class="nav-link"><img src="${path}"/> ${name}</a></li>`
           )
           .join('')}
      </ul>
   </div> `

  return Navbar
}
