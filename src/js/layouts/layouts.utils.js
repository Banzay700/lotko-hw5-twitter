import home from '../../assets/images/navIcons/home.svg'
import explore from '../../assets/images/navIcons/explore.svg'
import notifications from '../../assets/images/navIcons/notifications.svg'
import messages from '../../assets/images/navIcons/messages.svg'
import bookmarks from '../../assets/images/navIcons/bookmarks.svg'
import lists from '../../assets/images/navIcons/lists.svg'
import profile from '../../assets/images/navIcons/profile.svg'
import more from '../../assets/images/navIcons/more.svg'

export const navList = [
  { name: 'Home', path: home },
  { name: 'Explore', path: explore },
  { name: 'Notifications', path: notifications },
  { name: 'Messages', path: messages },
  { name: 'Bookmarks', path: bookmarks },
  { name: 'Lists', path: lists },
  { name: 'Profile', path: profile },
  { name: 'More', path: more },
]
