import { randomNum } from '../tools/randomNum'
import { Button } from '../UI/Button'
import { IMAGE_URL } from '../utils/constants'

const trendsContent = ['fury', 'Dapper Dan', 'Paul Ryan', 'TwitterDown']
const followContent = [
  { name: 'Paul Massaro', email: '@apmassaro3', avatar: IMAGE_URL + '/male/1.jpg' },
  { name: 'CoinMarketCap', email: '@CoinMarketCap', avatar: IMAGE_URL + '/male/2.jpg' },
  { name: 'Shib', email: '@Shibtoken', avatar: IMAGE_URL + '/male/3.jpg' },
]

const TrendsSection = () => {
  const TrendsSection = document.createElement('div')

  TrendsSection.innerHTML = `
      <input class="side-input" type="text" placeholder="Search Twitter"></input >
      <div class="side-trend">
      <span>Trends for you</span> 
      ${trendsContent
        .map(
          trend =>
            `<div class="side-card">Trending in Ukraine<i class="side-text">${trend}</i><i>${randomNum()} Tweets</i></div>`
        )
        .join('')}
      <span class="side-more">Show more</span>
      </div>`

  return TrendsSection
}

const FollowSection = () => {
  const Follow = document.createElement('div')
  Follow.className = 'side-follow'

  Follow.innerHTML = `
   <span>Who to follow</span>  
   ${followContent
     .map(({ name, email, avatar }) => {
       const followItem = `
   <div class="follow-item">
     <div class="item-info">
        <div class="item-img"><img src="${avatar}"></img></div>
        <div class="item-text"><i>${name}</i><i class="item-mail">${email}</i></div>
     </div >
      <div>${Button('Follow').outerHTML}</div>
   </div>`

       return followItem
     })
     .join('')}
   <span class="side-more">Show more</span>
   `
  return Follow
}

export const Sidebar = () => {
  const Sidebar = document.createElement('div')
  const container = document.createElement('div')

  Sidebar.className = 'sidebar'
  container.className = 'side-wrapper'

  container.append(TrendsSection(), FollowSection())
  Sidebar.append(container)

  return Sidebar
}
