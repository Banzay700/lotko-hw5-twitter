import PostCard from '../components/postCard'
import { request } from '../tools'

export class Post {
  constructor({ id, title, body, userId, name, email, avatar }) {
    this.id = id
    this.title = title
    this.body = body
    this.userId = userId
    this.name = name
    this.email = email
    this.avatar = avatar
    this.htmlElement = document.createElement('div')
  }

  addDeleteOption() {
    this.htmlElement.addEventListener('click', async e => {
      const url = `/posts/${this.id}`
      const deleteIcon = e.target.classList.contains('delete-icon')

      if (deleteIcon) {
        const { status } = await request({ url, method: 'DELETE' })
        status === 200 ? this.htmlElement.remove() : ''
      }
    })
  }

  renderPost() {
    this.addDeleteOption()

    return PostCard(this)
  }
}
