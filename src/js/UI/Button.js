export const Button = type => {
  const Button = document.createElement('button')
  Button.className = `button ${type.toLowerCase()}`
  Button.textContent = `${type}`

  return Button
}
