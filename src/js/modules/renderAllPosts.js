import { request, shuffle } from '../tools'
import { IMAGE_URL } from '../utils/constants'
import { Post } from '../classes/Post'

const getPostsInfo = async () => {
  const [{ res: users }, { res: posts }] = await Promise.all([
    request({ url: '/users' }),
    request({ url: '/posts' }),
  ])

  return posts.map(post => {
    const { id, name, email } = users[post.userId - 1]

    return { id: id, name: name, email: email, avatar: IMAGE_URL + `/female/${id}.jpg`, ...post }
  })
}

export const renderAllPosts = async () => {
  const allPostsInfo = await getPostsInfo()
  const postsList = document.querySelector('.post-list')

  shuffle(allPostsInfo).forEach(postInfo => {
    const post = new Post(postInfo)

    postsList.append(post.renderPost())
  })
}
