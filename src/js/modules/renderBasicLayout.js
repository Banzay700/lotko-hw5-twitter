import { Button } from '../UI/Button'
import { Home, Navbar, Sidebar } from '../layouts'

export const renderBasicLayout = () => {
  const app = document.querySelector('#app')

  app.prepend(Navbar(), Home(), Sidebar())

  const nav = document.querySelector('.nav')
  nav.append(Button('Tweet'))
}
