import { postIcons } from './components.utils'
import { randomNum } from '../tools'
import label from '../../assets/images/navIcons/label.svg'

const PostCard = ({ name, email, title, body, avatar, htmlElement }) => {
  htmlElement.className = `post-card`
  htmlElement.innerHTML = `
  <div class="post-avatar"><img src="${avatar}" /></div>
  <div class="post-content">
      <div class="post-username">${name} 
          <span class="post-label"> <img src="${label}"</span></div>
      <div class="post-email">${email}</div>
      <i class="delete-icon"></i>
      <div class="post-title">${title}</div>
      <div class="post-body">${body}</div>
      <ul class="post-actions">
        ${postIcons
          .map(
            ({ name, path }) =>
              `<li class="item" title=${name}><i class="icon">${path}</i>${randomNum()}</li>`
          )
          .join('')}
      </ul>  
  </div>`

  return htmlElement
}

export default PostCard
