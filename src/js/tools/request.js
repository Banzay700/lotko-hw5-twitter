import axios from 'axios'

import { API_URL } from '../utils/constants'

export const request = async ({ url, method = 'GET', body, headers } = {}) => {
  axios.defaults.baseURL = API_URL

  const fetchData = () => {
    if (method === 'GET') return axios.get(url, { params: body })
    else return axios({ url, method, data: body, headers })
  }

  try {
    const { data, status } = await fetchData()

    return { res: data, status }
  } catch ({ response }) {
    return { err: response }
  }
}
