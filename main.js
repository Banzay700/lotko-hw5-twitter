import './src/scss/style.scss'

import { renderBasicLayout, renderAllPosts } from './src/js/modules'

renderBasicLayout()
renderAllPosts()
